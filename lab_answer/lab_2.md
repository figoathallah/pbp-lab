1. Apakah perbedaan antara JSON dan XML?
- JSON lebih mudah dibaca dibanding XML
- JSON dapat menampilkan array, XML tidak bisa
- JSON berdasarkan Javascript, XML berdasarkan SGML
- JSON tidak meng-support namespaces, namun XML bisa
- JSON tidak memakai end tag, XML memakai end tag
- JSON kurang aman dibandingkan XML

2. Apakah perbedaan antara HTML dan XML?
- HTML merupakan markup language, XML merupakan markup language yang mendatakan markup language lain
- HTML fokus pada menampilkan data, XML fokus pada menyimpan data
- HTML bersifat static, sedangkan XML bersifat dynamic
- HTML tidak case-sensitive, XML case-sensitive
- HTML mempunyai tag yang predefined, sedangkan kita dapat mendefinisikan tag XML secara fleksibel 
- HTML ketika menemukan error kecil dapat diabaikan, XML tidak