from django.db import models

class Note(models.Model):
    recipient = models.CharField(max_length=30, default=" ")
    sender = models.CharField(max_length=30, default=" ")
    title = models.CharField(max_length=30, default=" ")
    message = models.TextField(max_length=500, default=" ")
