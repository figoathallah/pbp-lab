from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm


def index(request):
    notes = Note.objects.all().values()
    response = {"notes": notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    if request.method == "POST":
        form = NoteForm(request.POST or None)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/lab-4")

    else:
        form = NoteForm()

    response = {"form": form}
    return render(request, 'lab4_form.html', response)

def note_list(request):
    notes = Note.objects.all().values()
    response = {"notes": notes}
    return render(request, 'lab4_note_list.html', response)

