from django import forms
from django.forms.widgets import TextInput, Textarea
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ["recipient", "sender", "title", "message"]
        widgets = {"recipient": TextInput(attrs={'placeholder': 'Recipient name'}),
            "sender": TextInput(attrs={'placeholder': 'Sender name'}),
            "title": TextInput(attrs={'placeholder': 'Title of your message'}),
            "message": Textarea(attrs={'placeholder': "Enter your message"}),}